# ViaBill ePay Module  #
----------------------------
A module to be integrated in Magento WebShop for providing Payment Option and not a Payment Gateway.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
-----
- version: 1.0.1
- Module on BitBucket(https://bitbucket.org/ibilldev/viabill-epay-magento-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Module in to your Magento Web Shop to provide an option to pay using ViaBill through ePay Payment Gateway.

###Requirements###
------------
* PHP >= 5.5.0


###Compatibility###
-------------
* Magento =1.4, 1.5, 1.6, 1.7, 1.8, 1.9



###Integration Instructions :  Using Magento Connect ### 
-------------------------
1. Download the Module from the bitbucket. 
2. Extract the module and look for a zip file viz. Viabill_Payepay-1.0.1
3. Login to the Magento Admin Panel. Navigate to System->Magento Connect->Magento Connect Manager
4. Upload Package File : Browse and select the zip file to upload. 
   Please check the image below for reference :



5. Once upload is successful, navigate to System->Configuration->Payment Methods
6. Check the installed module and configure.
7. Done




###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 
2. Module contains two folders a.) app b.) media

*Figure shows the folder structure to place the necessary files for the module.***
![magento-epay-structure.PNG](https://bitbucket.org/repo/RR4yEa/images/649402178-magento-epay-structure.PNG)


3. Within the 'app' folder, follow the folder structure and place the folder named 'Viabill' inside your app/code/local.

4. Extract app/design/frontend/base/default/template/payepay and place it inside  your app/design/frontend/base/default/template/

5. Extract app/design/frontend/base/default/layout/payepay.xml and place it inside your 
   app/design/frontend/base/default/layout/

6. Extract app/etc/modules/Viabill_Payepay.xml and place it in your app/etc/modules/
   

7. Extract app/design/adminhtml/default/default/layout/payepay.xml and place it inside your app/design/adminhtml/default/default/layout/

8. Copy app/locale  folder into your app/ folder.

9. Inside the media folder, extract the "viabill" folder and place it in your magento media folder. The folder should be : media/viabill.

10. Clear the cache from Admin Panel: System->Configuration->Cache Management->Select All - Actions:Refresh - Submit. 

11. Logout from the admin panel and then login again.

12. Configure and activate the module under System->Configuration->Advanced. Check in Disable Modules Output -> Viabill_Payepay -> Enable. 

10. Go to Admin->System->Configuration->Payment Methods. Click On ViaBill betaling | ePay. 

11. Done.


#Uninstallation
--------------
1. Remove Folder "Viabill" from app/code/local
2. Remove viabill folder from media/
3. Remove the Viabill_Payeapy.xml file from app/etc/modules
4. Remove payepay folder from app/design/frontend/base/default/template/
5. Remove payepay.xml from your app/design/adminhtml/default/default/layout/
6. Remove payepay.xml from your app/design/frontend/base/default/layout/

#Automatic Uninstallation 
-----------------
1. Admin->System->Magento Connect->Magento Connect Manager
2. Look for Viabill_Payepay in installed modules section. 
3. Select Uninstall -- Commit Changes

![epay-magento-uninstall.PNG](https://bitbucket.org/repo/RR4yEa/images/1066436437-epay-magento-uninstall.PNG)




#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------

#Developer
---------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)