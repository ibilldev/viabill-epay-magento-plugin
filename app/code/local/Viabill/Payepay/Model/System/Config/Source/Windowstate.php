<?php

class Viabill_Payepay_Model_System_Config_Source_Windowstate
{

    public function toOptionArray()
    {
        return array(
            array('value'=>1, 'label'=>Mage::helper('adminhtml')->__('Overlay')),
            array('value'=>3, 'label'=>Mage::helper('adminhtml')->__('Full Screen')),
        );
    }

}