<?php
class Viabill_Payepay_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View {
    public function  __construct() {
				
		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
    	$row = $read->fetchRow("select * from viabillepay_order_status where orderid = '" . $this->getOrder()->getIncrementId() . "'");
    	
    	$standard = Mage::getModel('payepay/standard');
    	if (!$row || $row['status'] == '0') {
    		$this->_addButton('button_sendpaymentrequest', array('label' => Mage::helper('payepay')->__('Create payment request'), 'onclick' => 'setLocation(\'' . Mage::helper("adminhtml")->getUrl('payepay/adminhtml_paymentrequest/create/', array('id' => $this->getOrder()->getRealOrderId())) . '\')', 'class' => 'scalable go'), 0, 100, 'header', 'header');
		}
		
		parent::__construct();
    }
}