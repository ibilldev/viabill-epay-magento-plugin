<?php

class Viabil_Payepay_Block_Adminhtml_Form_Paymentrequest_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('paymentrequest_tabs');
		$this->setDestElementId('paymentrequest_form');
		$this->setTitle(Mage::helper('payepay')->__('Payment request'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general', array(
			'label' => Mage::helper('payepay')->__('General'),
			'title' => Mage::helper('payepay')->__('General'),
			'content' => $this->getLayout()->createBlock('payepay/adminhtml_form_paymentrequest_tab_general')->toHtml(),
		));
		
		$this->addTab('recipient', array(
			'label' => Mage::helper('payepay')->__('E-mail'),
			'title' => Mage::helper('payepay')->__('E-mail'),
			'content' => $this->getLayout()->createBlock('payepay/adminhtml_form_paymentrequest_tab_recipient')->toHtml(),
		));
		
		return parent::_beforeToHtml();
	}
}