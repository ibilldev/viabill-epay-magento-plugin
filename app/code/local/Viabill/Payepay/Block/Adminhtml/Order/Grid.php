<?php

class Viabill_Payepay_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareMassaction()
    {
		parent::_prepareMassaction();

        $this->getMassactionBlock()->addItem('payepay_order', array(
             'label'=> Mage::helper('sales')->__('Capture with ePay'),
             'url'  => $this->getUrl('payepay/adminhtml_massaction/payepayCapture'),
        ));

        return $this;
    }
}
?>