<?php

 
class Viabill_Payepay_Block_Adminhtml_Paymentrequest_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('paymentrequest_grid');
        $this->setDefaultSort('paymentrequestid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('payepay/paymentrequest')->getCollection()->setOrder('id', 'desc');
		$collection->getSelect()->where('status = ?', '1');
		
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('paymentrequestid', array(
            'header'    => Mage::helper('payepay')->__('ID'),
            'align'     => 'right',
            'width'     => '150px',
            'index'     => 'paymentrequestid',
        ));
		
		$this->addColumn('created', array(
            'header'    => Mage::helper('payepay')->__('Date'),
            'align'     => 'right',
            'width'     => '150px',
            'index'     => 'created',
			'type' 		=> 'datetime',
        ));
 
        $this->addColumn('orderid', array(
            'header'    => Mage::helper('payepay')->__('Order #'),
            'align'     => 'left',
            'index'     => 'orderid',
        ));
 
        $this->addColumn('amount', array(
            'header'    => Mage::helper('payepay')->__('Amount'),
            'align'     => 'left',
            'index'     => 'amount',
			//'type'		=> 'price',	
			'renderer'	=> new Viabill_Payepay_Block_Adminhtml_Paymentrequest_Renderer_Amount//divide by 100
        ));
 
        $this->addColumn('receiver', array(
            'header'    => Mage::helper('payepay')->__('Receiver'),
            'align'     => 'left',
            'index'     => 'receiver',
        ));
		
		$yesNoOptions = array('0' => Mage::helper('payepay')->__('No'), '1' => Mage::helper('payepay')->__('Yes'));
		
		$this->addColumn('ispaid', array(
            'header'    => Mage::helper('payepay')->__('Is Paid'),
            'align'     => 'left',
            'index'     => 'ispaid',
			'type'      => 'options',
			'options'   => $yesNoOptions,
        ));
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('id' => $row->getId()));
    }
}