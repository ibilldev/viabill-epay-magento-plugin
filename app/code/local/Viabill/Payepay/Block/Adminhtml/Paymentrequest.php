<?php
 
class Viabill_Payepay_Block_Adminhtml_Paymentrequest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		parent::__construct();
		$this->removeButton('add');
		$this->_controller = 'adminhtml_paymentrequest';
		$this->_blockGroup = 'payepay';
		$this->_headerText = Mage::helper('payepay')->__('Payment requests');
	}
	
	protected function _prepareLayout()
	{
		$this->setChild('grid', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_grid', $this->_controller . '.grid')->setSaveParametersInSession(true));
		return parent::_prepareLayout();
	}
}
