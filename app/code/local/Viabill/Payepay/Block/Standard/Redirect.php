<?php


class Viabill_Payepay_Block_Standard_Redirect extends Mage_Core_Block_Template
{
    
    public function __construct()
    {
        parent::__construct();
        $standard = Mage::getModel('payepay/standard');

        $this->setTemplate('payepay/standard/redirect_standardwindow.phtml');

        //
    	// Save the order into the epay_order_status table
    	//
    	$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->insert('viabillepay_order_status', array('orderid'=>$standard->getCheckout()->getLastRealOrderId()));
    }
}
