<?php
 
class Viabill_Payepay_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('payepay/standard/form.phtml');
        parent::_construct();
    }

    public function getDescription(){
        return trim(Mage::getStoreConfig('payment/payepay_standard/description'));
    }

    public function isShowIcon(){
        return Mage::getStoreConfig('payment/payepay_standard/show_icon');
    }
}