<?php


class Viabill_Payepay_Block_Sales_Order_Print extends Mage_Sales_Block_Order_Print
{
    protected function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('Print Order # %s', $this->getOrder()->getRealOrderId()));
        }
        $this->setChild(
            'payment_info',
            $this->helper('payment')->getInfoBlock($this->getOrder()->getPayment())
        );
    }

    public function getPaymentInfoHtml()
    {
        //return $this->getChildHtml('payment_info');
        
        $res = $this->getChildHtml('payment_info');
        
        //
				// Read info directly from the database   	
    		$read = Mage::getSingleton('core/resource')->getConnection('core_read');
    		$row = $read->fetchRow("select * from viabillepay_order_status where orderid = '" . $this->getOrder()->getIncrementId() . "'");
    		$standard = Mage::getModel('payepay/standard');
    		
    		if ($row['status'] == '1') {
	    		//
	    		// Payment has been made to this order
	    		$res .= "<table border='0' width='100%'>";
	    		if ($row['tid'] != '0') {
	    			$res .= "<tr><td>" . Mage::helper('payepay')->__('PAYEPAY_LABEL__19') . "</td>";
	    			$res .= "<td>" . $row['tid'] . "</td></tr>";
	    		}
	    		if ($row['cardid'] != '0') {
	    			$res .= "<tr><td>" . Mage::helper('payepay')->__('PAYEPAY_LABEL__26') . "</td>";
	    			$res .= "<td>" . $this->printLogo($row['cardid']) . "</td></tr>";
	    		}
	    		if (strlen($row['cardnopostfix']) != 0) {
	    			$res .= "<tr><td>" . Mage::helper('payepay')->__('PAYEPAY_LABEL__101') . "</td>";
	    			$res .= "<td>XXXX XXXX XXXX " . $row['cardnopostfix'] . "</td></tr>";
	    		}
	    		if ($row['transfee'] != '0') {
	    			$res .= "<tr><td>" . Mage::helper('payepay')->__('PAYEPAY_LABEL__27') . "</td>";
	    			$res .= "<td>" . $this->getOrder()->getBaseCurrencyCode() . "&nbsp;" . number_format(((int)$row['transfee']) / 100, 2, ',', ' ') . "</td></tr>";
	    		}
	    		$res .= "</table><br>";
	    		
	    	} else {
	    		$res .= "<br>" . Mage::helper('payepay')->__('PAYEPAY_LABEL__28') . "<br>";
	    	}
    	
    	return $res;
    }
    
    public function printLogo($cardid) {
    	$res = '<img src="';
    	
    	switch($cardid) {
    		case '1': {
    			$res .= $this->getSkinUrl('images/payepay/dankort.gif'); break;
    		}
    		case '2': {
    			$res .= $this->getSkinUrl('images/payepay/dankort.gif'); break;
    		}
    		case '3': {
    			$res .= $this->getSkinUrl('images/payepay/visaelectron.gif'); break;
    		}
    		case '4': {
    			$res .= $this->getSkinUrl('images/payepay/mastercard.gif'); break;
    		}
    		case '5': {
    			$res .= $this->getSkinUrl('images/payepay/mastercard.gif'); break;
    		}
    		case '6': {
    			$res .= $this->getSkinUrl('images/payepay/visaelectron.gif'); break;
    		}
    		case '7': {
    			$res .= $this->getSkinUrl('images/payepay/jcb.gif'); break;
    		}
    		case '8': {
    			$res .= $this->getSkinUrl('images/payepay/diners.gif'); break;
    		}
    		case '9': {
    			$res .= $this->getSkinUrl('images/payepay/maestro.gif'); break;
    		}
    		case '10': {
    			$res .= $this->getSkinUrl('images/payepay/amex.gif'); break;
    		}
    		case '12': {
    			$res .= $this->getSkinUrl('images/payepay/edankort.gif'); break;
    		}
    		case '13': {
    			$res .= $this->getSkinUrl('images/payepay/diners.gif'); break;
    		}
    		case '14': {
    			$res .= $this->getSkinUrl('images/payepay/amex.gif'); break;
    		}
    		case '15': {
    			$res .= $this->getSkinUrl('images/payepay/maestro.gif'); break;
    		}
    		case '16': {
    			$res .= $this->getSkinUrl('images/payepay/forbrugsforeningen.gif'); break;
    		}
    		case '17': {
    			$res .= $this->getSkinUrl('images/payepay/ewire.gif'); break;
    		}
    		case '18': {
    			$res .= $this->getSkinUrl('images/payepay/visa.gif'); break;
    		}
    		case '24': {
    			$res .= $this->getSkinUrl('images/payepay/mastercard.gif'); break;
    		}
    		case '25': {
    			$res .= $this->getSkinUrl('images/payepay/mastercard.gif'); break;
    		}
    	}
    	$res .= '" border="0" />';
    	return $res;
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    protected function _prepareItem(Mage_Core_Block_Abstract $renderer)
    {
        $renderer->setPrintStatus(true);

        return parent::_prepareItem($renderer);
    }

}

